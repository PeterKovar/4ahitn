import sqlite3

conn = sqlite3.connect('my.db')
cursor = conn.cursor()

#sql ='''CREATE TABLE IF NOT EXISTS students (
#     id INTEGER PRIMARY KEY,
#     name TEXT NOT NULL);'''
#sql ='''INSERT INTO students VALUES (1,'Ali');'''
sql ='''SELECT * FROM students;'''

cursor.execute(sql)
records = cursor.fetchall()
print('Die Zahl der Datensaetze ',len(records))
print('Daten:')
for row in records:
 print('Id ', row[0])
 print('Name ', row[1])
 print('\n')


conn.commit()

cursor.close()

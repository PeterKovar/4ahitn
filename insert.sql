CREATE TABLE IF NOT EXISTS students(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL);
  
INSERT INTO students VALUES
(100, 'Cleo'),
(101, 'Dea'),
(102, 'Emil'),
(103, 'Fritz'),
(104, 'Gusti'),
(105, 'Hansi');

